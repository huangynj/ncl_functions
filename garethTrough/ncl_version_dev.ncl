load "~/NCL/loadFiles.ncl"
load "~/NCL/functions/uv2cvF_Wrap.ncl"
load "~/NCL/functions/box_average/circle_avg.ncl"

uf = addfile("/cfsr/data/2012/u.2012.0p5.anl.nc","r")
vf = addfile("/cfsr/data/2012/v.2012.0p5.anl.nc","r")

tS = cd_inv_calendar(2012, 09, 07, 00, 00, 00,vf->time@units,0)
tE = cd_inv_calendar(2012, 09,07, 00, 00, 00,vf->time@units,0)

system("date")
u = uf->u({tS},{700},:,:)
v = vf->v({tS},{700},:,:)
u = circle_avg(u,5)
v = circle_avg(v,5)
u = where(ismissing(u), 0, u)
v = where(ismissing(v), 0, v)

;map = gsn_csm_contour_map(wks, v, mp)


sfvp = uv2sfvpF(u, v)
sfvp(0,:,:) = circle_avg(sfvp(0,:,:),3)
sfvp(0,:,:) = where(ismissing(sfvp(0,:,:)), 0, sfvp(0,:,:))

gradsf(sfvp(0,:,:), v, u )

map = gsn_csm_contour_map(wks, v, mp)

u = circle_avg(u,3)
v = circle_avg(v,3)
u = where(ismissing(u), 0, u)
v = where(ismissing(v), 0, v)

map = gsn_csm_contour_map(wks, v, mp)



u = where(ismissing(u), 0, u)
v = where(ismissing(v), 0, v)

cu = u
cu= uv2cvF_Wrap(u,v)   ; This step will take like an hour for a season of cfsr

cu = circle_avg(cu,6)


cu = where(ismissing(cu),0,cu)


ddxcu = cu
ddycu = cu
gradsf(cu, ddxcu, ddycu)
advcu= -(u*ddxcu)-(v*ddycu)

delete( [/ddxcu, ddycu/])

ddxadv = cu
ddyadv = cu
gradsf(advcu, ddxadv, ddyadv)
advadvcu = (u*ddxadv)+(v*ddyadv)

smoothadvout = advcu
smoothadvout = where(cu.gt.0.25, smoothadvout, smoothadvout@_FillValue)
smoothadvout = where(advadvcu.gt.0., smoothadvout, smoothadvout@_FillValue)
;smoothadvout = where(u.lt.0., smoothadvout, smoothadvout@_FillValue)
system("date")

