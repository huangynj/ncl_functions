;==================================================================
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "~/NCL/functions/trough.ncl"


begin


; ----------------- set up vars for troughing ---------------------------

globflag =1
smooth = 3 ;was 10
vortmin = 0.
ucutoff = 8.    ; jet
ucutoff2 = 0.   ; min background wind (magnitude) in which to define troughs
; ----- area boundaries

maxlat = 50.
minlat = -10.
maxlon = 50.
minlon = -50.


; ---------------------- input data ----------------------------

uf = addfile("/ecmwf/interim/2010/u.201007.grib","r")
vf = addfile("/ecmwf/interim/2010/v.201007.grib","r")


do tt=0,0

utestin = lonFlip(uf->U_GDS0_ISBL(tt,{700},:,:))     ; erai data goes 0->360
vtestin = lonFlip(vf->V_GDS0_ISBL(tt,{700},:,:))

vtest = vtestin({minlat:maxlat},{minlon:maxlon})
utest = utestin({minlat:maxlat},{minlon:maxlon})


   utest!0 = "lat"
   utest!1 = "lon"
   utest@_FillValue = -9999.

   vtest!0 = "lat"
   vtest!1 = "lon"
   vtest@_FillValue = -9999.

   utest2 = utest
   vtest2 = vtest

printVarSummary(utest)


; ----------------- troughs ---------------------

troff = gtrough( utest, vtest, vortmin, ucutoff,ucutoff2, smooth, globflag)
printVarSummary(troff)

end do


end