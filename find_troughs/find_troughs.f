       subroutine search_eastwest(cdata, outdata, thiswave,nx, ny,i, jj)
        real cdata(nx,ny), outdata(nx,ny)
        integer nx, ny, jj, i, ii, thiswave
        
            do ii=1,10
               if(cdata(i+ii,jj).gt.0.5)then
               outdata(i+ii,jj) = thiswave
               else
               goto 210
               end if
           enddo
210     continue
           do ii=1,10
               if(cdata(i-ii,jj).gt.0.5)then
               outdata(i-ii,jj) = thiswave
               else
               goto 211
               end if
           enddo
211       continue
        return
        end







C   NCLFORTSTART
        subroutine find_troughs(cdata, outdata, nx, ny, waves)
        integer nx, ny,ii,j,i,waves, thiswave
        real indata(nx, ny),outdata(nx, ny),cdata(nx,ny)
C   NCLEND
        thiswave = waves
        outdata = 0
        do j=2, ny-1  
            do i=2,nx
c             if( indata(i-1,j)<0 .and. indata(i+1,j)>0) then   
              if(cdata(i,j).gt.0.5)then
                if( (outdata(i-1, j).ne.0))then
                    thiswave = int(outdata(i-1, j) )
                else if(outdata(i-1, j-1).ne.0)then
                    thiswave = int(outdata(i-1, j-1) )
                else if(outdata(i, j-1).ne.0)then
                    thiswave = int(outdata(i, j-1) )
                else if(outdata(i+1, j-1).ne.0)then 
                    thiswave = int(outdata(i+1, j-1) )
                else if(outdata(i+1, j-2).ne.0)then 
                    thiswave = int(outdata(i+1, j-2) )
        call search_eastwest(cdata, outdata, thiswave, nx, ny,i, j-1)
                else if(outdata(i-1, j-2).ne.0)then 
                    thiswave = int(outdata(i-1, j-2) )
        call search_eastwest(cdata, outdata, thiswave, nx, ny,i, j-1)
                else if(outdata(i, j-2).ne.0)then 
                    thiswave = int(outdata(i, j-2) )
        call search_eastwest(cdata, outdata, thiswave, nx, ny,i, j-1)
                
                else
                   thiswave = waves
                   waves = waves+1
                end if      


          call search_eastwest(cdata, outdata, thiswave, nx, ny,i, j)

               end if
C              end if
            enddo
        enddo
        
        do j=2, ny-1  
          do i=2,nx
            if( (outdata(i,j) .eq. 0) .and. (outdata(i, j-1).ne.0)
     &  .and. ( outdata(i, j-1) .eq. outdata(i, j+1) )  )then
                outdata(i,j) = outdata(i,j-1)
            end if
          enddo
        enddo
        
        do j=2, ny-1  
          do i=2,nx
            if( (outdata(i,j) .eq. 0) .and. (cdata(i, j).gt.0.5)
     &  .and. ( outdata(i, j-1).ne.0)  )then
                  thiswave = int(outdata(i, j-1) )
        call search_eastwest(cdata, outdata, thiswave, nx, ny,i, j)
            end if
          enddo
        enddo
        
        do j=2, ny-1  
          do i=2,nx
            if( (outdata(i,ny-j) .eq. 0) .and. (cdata(i, ny-j).gt.0.5)
     &  .and. ( outdata(i, ny-j-1).ne.0)  )then
                  thiswave = int(outdata(i, ny-j-1) )
        call search_eastwest(cdata, outdata, thiswave, nx, ny,i, ny-j)
            end if
          enddo
        enddo

c        print *,"Number of Waves found=", waves
        return
        end
        
