C   NCLFORTSTART
        subroutine find_troughs(cdata, outdata, nx, ny, waves, thresh)
        integer nx, ny,ii,j,i,waves, thiswave
        real indata(nx, ny),outdata(nx, ny),cdata(nx,ny), thresh
        integer neighbours(8)
C   NCLEND
        thiswave = waves
        outdata = 0
        do j=2, ny-1  
            do i=2,nx
              if(cdata(i,j).gt.thresh)then
                neighbours = 0
                ct=0
                    neighbours(1) = int(outdata(i-1, j+1) )
                    neighbours(2) = int(outdata(i-1, j  ) )
                    neighbours(3) = int(outdata(i-1, j-1) )
                    neighbours(4) = int(outdata(i  , j-1) )
                    neighbours(5) = int(outdata(i+1, j-1) )
                    neighbours(6) = int(outdata(i+1, j   ) )
                    neighbours(7) = int(outdata(i+1, j+1) )
                    neighbours(8) = int(outdata(i  , j+1) )

                    ct = COUNT(neighbours.ne.0)
                    
                if(ct.eq.1)then
                    do n=1,8
                        if( neighbours(n) .ne. 0 ) then            
                            outdata(i,j) = neighbours(n)
                        end if
                    end do 
                else if(ct.gt.1)then
                   outdata(i,j) = thiswave
                    do n=1,8
                        if( neighbours(n) .ne. 0 ) then    
                WHERE( outdata.eq.neighbours(n)) outdata = thiswave
                        end if
                    end do
                    thiswave = thiswave + 1
                else
                   outdata(i,j) = thiswave
                   thiswave = thiswave + 1
                end if
        
            end if
            enddo
        enddo
        
c       clear out anything less than 3 pixels and renumber sequentially
        new_waves = waves
        do w= MAX( MINVAL(outdata), 1.),  MAXVAL(outdata)
            ct= COUNT(outdata.eq.w)
            if(ct.gt.0)then
              if(ct.lt.3)then
                WHERE( outdata .eq. w ) outdata = 0.
              else
                WHERE( outdata .eq. w ) outdata = new_waves
                new_waves = new_waves+1
             end if
            end if
        end do
        
        return
        end
        
