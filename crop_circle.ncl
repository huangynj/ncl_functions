;load "~/NCL/loadFiles.ncl"




;================================
; function to mask all data outside of a circle
; Pass function the 2d Data array
; Pass a central Latitude and longitude of the circle
; and a Radius of the circle in degrees, 
; "Circular" ability will depend on data as will central location
; returns data with all metadata it arrived with
; Written By Alan Brammer
;=======================================
undef("crop_circle")
function crop_circle(inData, clat,clon,radius)
local inData, lonIn, delta_x, r, clati, cloni, mlon,lat, y
begin

; For Masking region Latitude needs to be increasing
inData!0 = "lat"
if(inData&lat(1).lt.inData&lat(0))then
  print("******* ERROR LATITUDE ARRAY NEEDS TO BE INCREASING ************")
  exit
end if


; I like to work on Africa so think things should be -180:180 
inData!1 = "lon"
if(inData&lon(0).eq.0)then
  inData = lonFlip(inData)
end if

; Determine delta x for switching Radius in degrees to indices 
lonIn = inData&lon
delta_x = lonIn(1)-lonIn(0)




; r in indices

r = floattoint(floor(radius/delta_x))  

; define central lat and lon of circle
clati = ind_nearest_coord(clat,inData&lat,0)
cloni = ind_nearest_coord(clon,inData&lon,0)


; 0/4 of the circle points loop through below to make rest of circle
mlon = new(r,integer)
do y = 0,r-1
mlon(y)= round(sqrt(r^2-y^2),3)   ; round to integer
end do 

do lat=0,r-1
inData(clati+lat,(cloni+mlon(lat)):) = (/ inData@_FillValue /)
inData(clati-lat,(cloni+mlon(lat)):) = (/ inData@_FillValue /)
inData(clati+lat,:(cloni-mlon(lat))) = (/ inData@_FillValue /)
inData(clati-lat,:(cloni-mlon(lat))) = (/ inData@_FillValue /)
inData((clati+r):,:) = (/  inData@_FillValue /)
inData(:(clati-r),:) = (/  inData@_FillValue /)
end do
return(  inData )

end



undef("draw_circle")
function draw_circle(inData, clat,clon,radius)
begin


; For Masking region Latitude needs to be increasing
inData!0 = "lat"
if(inData&lat(1).lt.inData&lat(0))then
  print("******* ERROR LATITUDE ARRAY NEEDS TO BE INCREASING ************")
  exit
end if


; I like to work on Africa so think things should be -180:180 
inData!1 = "lon"
if(inData&lon(0).eq.0)then
  inData = lonFlip(inData)
end if

; Determine delta x for switching Radius in degrees to indices 
lonIn = inData&lon
if(.not.isatt(inData&lon,"delta_x"))then
  inData&lon@delta_x = lonIn(1)-lonIn(0)
  print("delta x of "+inData&lon@delta_x +" being used")
end if

; r in indices

r = floattoint(2*radius/inData&lon@delta_x)  

; define central lat and lon of circle
clati = ind_nearest_coord(clat,inData&lat,0)
cloni = ind_nearest_coord(clon,inData&lon,0)


; 1/4 of the circle points loop through below to make rest of circle
mlon = new(r,integer)
do y = 0,r-1
mlon(y)= round(sqrt(r^2-y^2),3)   ; round to integer
end do 


inData = 0
; loop through each quandrant of the circle and then N and S of circle to mask
do lat=0,r-1
inData(clati+lat,(cloni+mlon(lat))) = 1
inData(clati-lat,(cloni+mlon(lat))) = 1
inData(clati+lat,(cloni-mlon(lat))) = 1
inData(clati-lat,(cloni-mlon(lat))) = 1
end do

return(inData)

end



undef("test_crop_circle")
procedure test_crop_circle()
begin

 inFile  = addfile("/ecmwf/interim/2010/u.201007.grib" ,"r")
inData = inFile->U_GDS0_ISBL(0,0,::-1,:)

; inFile  = addfile("/ct13/janiga/data/trmm/3b42/trmm3b42.raw.nc","r")
; inData = inFile->trmm3b42(0,:,:)

clat = 5   ; North
clon = -5   ; East West
radius = 5  ; degrees



inData = crop_circle(inData,clat,clon,radius)


wks = gsn_open_wks("x11","temp")

res = True

res@mpMaxLatF = 15
res@mpMinLatF = -15
res@mpMaxLonF = 15
res@mpMinLonF = -15

res@cnFillOn = True
plot = gsn_csm_contour_map(wks, inData, res)

printVarSummary(inData)


end
