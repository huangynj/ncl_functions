#!/bin/bash
source ~/.cshrc

echo "-----------------------------------------------------"
echo "-----------------------------------------------------"
echo ""
echo " "
echo " Converts an image of a color table to an rgb triplets file"
echo " Needs to be a vertical colormap image with a clear route down the centre"
echo " Utilises gdal_translate so will fail without this program."
input=$1
name=$2


echo " Input file: "$input
echo " Output file: "$name".rgb"

gdal_translate -ot Int16 -of netCDF -expand rgb $input $name.nc

if [ $? -eq 0 ]
then
ncl -Q name='"'$name'"' /Volumes/acacia/NCL/functions/convert_colormap/nc2rgb.ncl 
fi

if [ $? -eq 0 ]
then
ncl -Q name='"'$name'"' /Volumes/acacia/NCL/functions/convert_colormap/preview_rgb.ncl 
fi



