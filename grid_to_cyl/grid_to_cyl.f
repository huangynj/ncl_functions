        function toradians(x)
        real x, pi
        pi=(355./113.) ! pi Approx good to 6 decimal places
        toradians = x * (pi/180.)
        return
        end function toradians

        function todegrees(x)
        real x, pi
        pi=(355./113.) ! pi Approx good to 6 decimal places
        todegrees = x * (180./pi)
        return
        end function todegrees


        function degrees_dist_m(inlat, inlon, rlat, rlon )
        real inlat, inlon, rlat, rlon, R, pi
        real r_lat, r_rlat, r_lon, r_rlon, dy, dx
        real a, c, d

         R=6371. ! Earth radius km.
         pi=(355./113.) ! pi Approx good to 6 decimal places

        r_lat = toradians(inlat)
        r_rlat = toradians(rlat)
        r_lon = toradians(inlon)
        r_rlon = toradians(rlon)
        dy = (r_lat - r_rlat )
        dx = (r_lon - r_rlon)

        a=sin(dy/2)*sin(dy/2)+cos(r_lat)*cos(r_rlat)*sin(dx/2)*sin(dx/2)
        c= 2 * atan2( sqrt( a ) , sqrt( 1 - a) )
        d = R * c

        degrees_dist_m = d
        return
        end function degrees_dist_m


        real function bearing(inlat, inlon, rlat, rlon)
        real inlat, inlon, rlat, rlon, R, pi
        real r_lat, r_rlat, r_lon, r_rlon, dy, dx
         R=6371. ! Earth radius km.
         pi=(355./113.) ! pi Approx good to 6 decimal places

        r_lat = toradians(inlat)
        r_rlat = toradians(rlat)
        r_lon = toradians(inlon)
        r_rlon = toradians(rlon)

        bearing = ATAN2(SIN(r_rlon-r_lon)*COS(r_rlat),
     +  COS(r_lat)*SIN(r_rlat)-SIN(r_lat)*COS(r_rlat)*COS(r_rlon-r_lon))
        return
        end



C NCLFORTSTART
        subroutine distance_angles( inlat, inlon, rlat, rlon, d, a,n)
        real inlat(n), inlon(n), rlat(n), rlon(n),d(n),a(n)
        real  R, pi, dy, dx
        real totdist, xdist, ydist, bear, r2d
        integer nn, n
C NCLEND
        r2d = ( 45.0/atan(1.) )  ! north is 0degree
        do nn=1,n
           dy =  (rlat(nn) - inlat(nn))
           dx =  (rlon(nn) - inlon(nn))
           a(nn) = r2d * atan2( dy, dx )

         d(nn)=degrees_dist_m(inlat(nn), inlon(nn), rlat(nn), rlon(nn) )
        end do
        end


C NCLFORTSTART
       subroutine radial_azi_regrid(grid, ny, nx,lats,lons,
     +  retval, nr, na, radbins, azibins, rlat, rlon, mv)
        real lats(ny), lons(nx), rlat, rlon
        real grid(nx, ny), retval(na,nr), radbins(nr), azibins(na)
        real angl, dist
        integer ii, jj,  tt, rr, na, nr
        real mv, mdist,cnts(na,nr), bin1, bin2
C NCLEND
        retval = 0.
        cnts   = 0

      do jj = 1, ny         ! Start lat/ lon loop
       do ii = 1, nx

        if(grid(ii,jj) .eq. mv)then
            cycle
        end if

        !Compute distance and angle with respect to the center
      call distance_angles(lats(jj), lons(ii), rlat, rlon, dist, angl,1)

        if (angl .lt. 0) then
          angl = angl+360.
        else if (angl .ge. 360.) then
          angl = angl-360.
        end if

        !Bin into radial bins
        do rr = 1, nr-1   ! loop through if greater than last and less then next
          if (dist .ge. radbins(rr) .and. dist .lt. radbins(rr+1) ) then
          do tt = 1, na
            bin1 = azibins(tt)
            if(tt.eq.na)then
             bin2 = 360
            else
             bin2 = azibins(tt+1)
            end if
           if(angl.ge.bin1 .and. angl.lt.bin2 )then
             retval(tt,rr) = retval(tt,rr)+grid(ii,jj)
             cnts(tt,rr)      = cnts(tt,rr) + 1
c            exit    ! found no point looping anymore
            end if
          end do    ! azim angle loop
c          exit      ! if we hit the radial bin then stop looping
          end if
        end do  !   radial distance loop

       end do    ! ii loop
      end do      ! jj loop

        do rr = 1, nr ! loop through if greater than last and less then next
          do tt = 1, na
            if(cnts(tt,rr).gt.0)then
            retval(tt,rr) = retval(tt,rr) / cnts(tt,rr)
            else
            retval(tt,rr) = mv
            end if
          end do
        end do
        return
        end
