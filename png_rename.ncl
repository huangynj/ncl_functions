;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "~/NCL/functions/garethTrough/local_files/garethtrough.ncl"


procedure png_rename(root, image_name, imax)
begin
print("beginning function")
;root = "~/WEB/sf_pv_time/ECMWF/filtered/2005/"
;image_name = "sf_pv_overlay-"
;imax =366
if (imax .eq. 0) then

system("mv "+root+image_name+".000001.png "+root+image_name+".png")
else
do i=0,imax


system("mv "+root+image_name+i+".000001.png "+root+image_name+i+".png")

end do
end if
print("png renamed")
end



procedure ping()
begin

quote = str_get_dq()
system( "/usr/bin/printf "+ quote + "\a" + quote  )

end


;##################################################################
;###### Calculate objective trough and jet ########################
;##################################################################

function calc_obj(u,v,smoothb)  
begin

  globflag = 1
  smooth   =  0 
  vortmin  = 0.125
  ucutoff  = 12.
  ucutoff2 = 0.

  lon_r = fspan(0,358,180)
  lat_r = fspan(-90,90,91)

  lon_r@units = "degrees_east"
  lat_r@units = "degrees_north"

  ure  = area_hi2lores_Wrap (u&lon,u&lat,u,True,1,lon_r,lat_r,False)  
  vre  = area_hi2lores_Wrap (v&lon,v&lat,v,True,1,lon_r,lat_r,False) 

  ure!0 = "lat"
  ure!1 = "lon"

  ure&lat = lat_r
  ure&lon = lon_r

  copy_VarMeta(ure,vre)
  
  if(smoothb) then
    vrt = (/uv2vrF(ure(:,:),vre(:,:))/)          ;### Non-divergent rel. vorticity
    vrt = vrt
    ur = ure
    vr = vre
  else
    vrt = (/uv2vrF(u(:,:),v(:,:))/)          ;### Non-divergent rel. vorticity
    vrt = vrt
    ur = u
    vr = v
  end if

  vr2uvf(vrt,ur,vr) 

  ur!0 = "lat"
  ur!1 = "lon"

  if(smoothb) then
    ur&lat = lat_r
    ur&lon = lon_r

    wrf_smooth_2d(ur,3)
    wrf_smooth_2d(vr,3)
  else
    ur&lat = u&lat
    ur&lon = u&lon
  end if
 
  copy_VarMeta(ur,vr)

  obj = gtrough(ur(:,:),vr(:,:),vortmin,ucutoff,ucutoff2,smooth,globflag)

  obj@_FillValue=-9999.

  return(obj)

end



function calc_obj_HR(u,v,smoothb)  
begin

  globflag = 1
  smooth   =  0 
  vortmin  = 0.125
  ucutoff  = 12.
  ucutoff2 = 0.

  lon_r = fspan(0,359.5,720)
  lat_r = fspan(-90,90,361)

  lon_r@units = "degrees_east"
  lat_r@units = "degrees_north"

  ure  = area_hi2lores_Wrap (u&lon,u&lat,u,True,1,lon_r,lat_r,False)  
  vre  = area_hi2lores_Wrap (v&lon,v&lat,v,True,1,lon_r,lat_r,False) 

  ure!0 = "lat"
  ure!1 = "lon"

  ure&lat = lat_r
  ure&lon = lon_r

  copy_VarMeta(ure,vre)
  
  if(smoothb) then
    vrt = (/uv2vrF(ure(:,:),vre(:,:))/)          ;### Non-divergent rel. vorticity
    vrt = vrt
    ur = ure
    vr = vre
  else
    vrt = (/uv2vrF(u(:,:),v(:,:))/)          ;### Non-divergent rel. vorticity
    vrt = vrt
    ur = u
    vr = v
  end if

  vr2uvf(vrt,ur,vr) 

  ur!0 = "lat"
  ur!1 = "lon"

  if(smoothb) then
    ur&lat = lat_r
    ur&lon = lon_r

    wrf_smooth_2d(ur,3)
    wrf_smooth_2d(vr,3)
  else
    ur&lat = u&lat
    ur&lon = u&lon
  end if
 
  copy_VarMeta(ur,vr)

  obj = gtrough(ur(:,:),vr(:,:),vortmin,ucutoff,ucutoff2,smooth,globflag)

  obj@_FillValue=-9999.

  return(obj)

end
