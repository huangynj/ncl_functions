C   NCLFORTSTART
        subroutine background_flow(indata, bkgrd, nx,ny, dx, dy)
        real indata(nx,ny), bkgrd(nx,ny)
        integer nx, ny
        integer rx, ry
        real dx, dy
C NCLEND

        print *," * * * * * * * * * * * * "
        print *,"* * * * * * * * * * * * "
        print *," * * * * * * * * * * * * "
        print *,"  Box Average Routine "
        print *," * * * * * * * * * * * * "
        print *," Does not work near the boundaries"
        print *," Input Parameters: "
        print *," nlon =", nx ," nlat =",ny
        print *," width =",dx, "height =",dy

        rx = (dx*nx)/360
        print *, rx
        ry = (dy*(ny-1))/180
        print *, dx," = ",rx,"grid points"

        do i=(rx+1), (nx-rx)
            do j=ry, (ny-ry)
        bkgrd(i,j) = sum( indata( (i-rx):(i+rx),(j-ry):(j+ry) ) )
            enddo
        enddo

        bkgrd = bkgrd /( ((2*rx)+1)*((2*ry)+1) )

        return
        end

C   NCLFORTSTART
        subroutine circle_avg_old(indata, bkgrd, nx,ny,dx)
        real indata(nx,ny), bkgrd(nx,ny), tempv
        integer nx, ny,cx ,i,j,y
        integer rx, ry, tot_cx, divider
        real dx
C NCLEND

        print *," * * * * * * * * * * * * "
        print *,"* * * * * * * * * * * * "
        print *," * * * * * * * * * * * * "
        print *,"  Circle Average Routine "
        print *," * * * * * * * * * * * * "
        print *," Does not work near the boundaries"
        print *," Input Parameters: "
        print *," nlon =", nx ," nlat =",ny
        print *," radius = ",dx," grid squares only at the moment"


c        rx = (dx*nx)/360
c        print *, rx
c        ry = (dy*(ny-1))/180
c        print *, dx," = ",rx,"grid points"

c       cx defines the longitudinal grid points for each lat.
c       This could be adaptable to radius, should be do able.

        tot_cx = 0
        do y=1, dx
        tot_cx = tot_cx + int( sqrt( dx**2 - y**2 )+0.5)
        end do
        divider = (tot_cx*4) -1


        do i=(dx+1),(nx-dx-1)
            do j=(dx+1),(ny-dx-1)
            tempv = 0.0
            y=1
            cx =   int( sqrt( dx**2 - y**2 )+0.5)
            tempv = tempv + sum( indata( (i-cx):(i+cx) , (j+(y-1))  ))
                do y=2,dx
                cx =   int( sqrt( dx**2 - y**2 )+0.5)
        tempv = tempv + sum( indata( (i-cx):(i+cx) , (j+(y-1))  ))
        tempv = tempv + sum( indata( (i-cx):(i+cx) , (j-(y-1))  ))
                enddo
       bkgrd(i,j) = tempv / divider
        tempv = 0.0
            enddo
        enddo
        return
        end


C   NCLFORTSTART
        subroutine circle_avg(indata, bkgrd, nx,ny,dx)
        real indata(nx,ny), bkgrd(nx,ny), tempv
        integer nx, ny,cx ,i,j,y, ii, iii, dxi
        integer rx, ry, tot_cx, divider
        real dx
C NCLEND

c        print *," * * * * * * * * * * * * "
c        print *,"* * * * * * * * * * * * "
c        print *," * * * * * * * * * * * * "
c        print *,"  Circle Average Routine "
c        print *," * * * * * * * * * * * * "
c        print *,"Loops Over the Cyclic but not Poles"
c        print *," Input Parameters: "
c        print *," nlon =", nx ," nlat =",ny
c        print *," radius = ",dx," grid squares only at the moment"
        dxi = int(dx) ! need an integer version of dx

c        rx = (dx*nx)/360
c        print *, rx
c        ry = (dy*(ny-1))/180
c        print *, dx," = ",rx,"grid points"

c       cx defines the longitudinal grid points for each lat.

        tot_cx = 0
        do y=1, dx
        tot_cx = tot_cx + int( sqrt( dx**2 - y**2 )+0.5)
        end do
        divider = (tot_cx*4) -1


        do i=1,nx
            do j=(dx+1),(ny-dx-1)
            tempv = 0.0
            y=1
            cx =   int( sqrt( dx**2 - y**2 )+0.5)
            tempv = tempv + sum( indata( (i-cx):(i+cx) , (j+(y-1))  ))
                do y=2,dx
                cx =   int( sqrt( dx**2 - y**2 )+0.5)
                    do ii=(i-cx), (i+cx)
                        iii = ii
                        if(iii.lt.1)then
                            iii = iii+nx
                        end if
                        if(iii.gt.nx)then
                            iii = iii-nx
                        end if
        tempv = tempv + indata( iii , (j+(y-1)) )
        tempv = tempv + indata( iii , (j-(y-1)) )
                    end do
                enddo
       bkgrd(i,j) = tempv / divider
        tempv = 0.0
            enddo
        enddo
        bkgrd(:,1:dxi) = 9.96921e+36
        bkgrd(:,(ny-dxi):ny) = 9.96921e+36  !This if NCLs Float Missing Val.
        return
        end


C   NCLFORTSTART
        subroutine circle_avg_m(indata, bkgrd, inlat, nx,ny,dxm)
        real indata(nx,ny), bkgrd(nx,ny), tempv
        real lat(ny), inlat(ny), divider
        integer nx, ny,cx ,i,j,y, ii, iii, dxi,dxmi
        integer rx, ry, tot_cx
        real dxm
C NCLEND
c
c       Last updated: April 22nd 2013
c       Considers latitude in size/shape of circle and weigths for average.
c       Potential error in that I use simple Trig for circle distances.
c
         R=6371. ! Earth radius km.
         pi=(355./113.) ! pi Approx good to 6 decimal places

         dxmi = int(dxm) ! need an integer version of dx
c       cx defines the longitudinal grid points for each lat.

          dx  = int(  dxmi/(2*pi*(R/nx)) )
          dxi = int(dx)  ! not sure why this is needed but indexing fails without it.

c      Convert latitude to radians and cos(lat)
         lat = inlat   ! work with temp variable so as to not overwrite.
         lat = lat*(pi/180.)   ! convert to radians
         lat = cos(lat)         ! Take cos of radians


        do i=1,nx  ! Loop across longitudes
            do j=(dx+1),(ny-dx-1)  ! loop lats excluding radius of circle
            tempv = 0.0
            y=0
            divider = 0.0
            do y=-dx+1,dx-1 ! work way up circle
              lat1 = lat(j)  !  centre of circle
              lat2 = lat(j+y) ! vertical distance from cirlce centre.
              xr=acos(-((sin(lat1)*sin(lat2))&
               &-cos(dxmi/R))/(cos(lat1)*cos(lat2)))
              xd = int( xr*(pi/180.)/(360/nx) )
              cx = xd ! conv. to grid points.
                 do ii=(i-cx), (i+cx)  ! work across circle
                        iii = ii
                        if(iii.lt.1)then ! loop over break assumes global w/o cyclic
                            iii = iii+nx
                        end if
                        if(iii.gt.nx)then
                            iii = iii-nx
                        end if
                  tempv = tempv + (lat(j+y))*indata( iii , (j+y) )
                  divider = divider + (lat(j+y))
                    end do
             end do
        bkgrd(i,j) =  tempv/divider
        tempv = 0.0
            enddo
        enddo
        bkgrd(:,1:dxi) = indata(:,1:dxi)  ! Fill poles with unsmoothed data.
        bkgrd(:,(ny-dxi):ny) = indata(:,(ny-dxi):ny)
        return
        end
