C   NCLFORTSTART
        subroutine circle_avg_m(indata, bkgrd, inlat, nx,ny,dxm)
        real indata(nx,ny), bkgrd(nx,ny), tempv, R
        real lat(ny), inlat(ny), divider, lat1, lat2
        integer nx, ny,cx ,i,j,y, ii, iii, dxi,dxmi
        integer rx, ry, tot_cx
        real dxm, clat(ny)
C NCLEND
c
c       Last updated: April 22nd 2013
c       Considers latitude in size/shape of circle and weigths for average.
c       Potential error in that I use simple Trig for circle distances.
c
         R=6371. ! Earth radius km.
         pi=(355./113.) ! pi Approx good to 6 decimal places

         dxmi = int(dxm) ! need an integer version of dx
c       cx defines the longitudinal grid points for each lat.

          dx  = int(  dxmi/(2*pi*(R/nx)) )
          dxi = int(dx)  ! not sure why this is needed but indexing fails without it.

c      Convert latitude to radians and cos(lat)
         lat = inlat   ! work with temp variable so as to not overwrite.
         lat = lat*(pi/180.)   ! convert to radians
         clat = cos(lat)         ! Take cos of radians


        do i=(dx+1),(nx-dx-1)  ! Loop across longitudes
            do j=(dx+2),(ny-dx-2)  ! loop lats excluding radius of circle
            tempv = 0.0
            y=0
            divider = 0.0
            do y=-dx,dx ! work way up circle
              lat1 = lat(j)  !  centre of circle
              lat2 = lat(j+y) ! vertical distance from cirlce centre.
              xr=acos(-((sin(lat1)*sin(lat2))
     +         -cos(dxmi/R))/(cos(lat1)*cos(lat2)))
              xd = int( xr/(pi/180.)/(360./nx) )
              cx = xd ! conv. to grid points.
                 do ii=(i-cx), (i+cx)  ! work across circle
                        iii = ii
                        if(iii.lt.1)then ! loop over break assumes global w/o cyclic
                            iii = iii+nx
                        end if
                        if(iii.gt.nx)then
                            iii = iii-nx
                        end if
                  tempv = tempv + (clat(j+y))*indata( iii , (j+y) )
                  divider = divider + (clat(j+y))
                    end do
             end do
        bkgrd(i,j) =  tempv/divider
        tempv = 0.0
            enddo
        enddo
        bkgrd(:,1:(dxi+2)) = indata(:,1:(dxi+2))  ! Fill poles with unsmoothed data.
        bkgrd(:,(ny-dxi-2):ny) = indata(:,(ny-dxi-2):ny)
        return
        end
