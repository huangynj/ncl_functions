C   NCLFORTSTART
        subroutine circle_avg(indata, bkgrd, nx,ny,dx)
        real indata(nx,ny), bkgrd(nx,ny), tempv
        integer nx, ny,cx ,i,j,y, ii, iii, dxi
        integer rx, ry, tot_cx, divider
        real dx
C NCLEND

c        print *," * * * * * * * * * * * * "
c        print *,"* * * * * * * * * * * * "
c        print *," * * * * * * * * * * * * "
c        print *,"  Circle Average Routine "
c        print *," * * * * * * * * * * * * "
c        print *,"Loops Over the Cyclic but not Poles"
c        print *," Input Parameters: "
c        print *," nlon =", nx ," nlat =",ny
c        print *," radius = ",dx," grid squares only at the moment"
        dxi = int(dx) ! need an integer version of dx
c
c       Does not take into account cos(lat). so needs adaptation before using
c                 out side of tropics!!!!!
c
c        rx = (dx*nx)/360
c        print *, rx
c        ry = (dy*(ny-1))/180
c        print *, dx," = ",rx,"grid points"

        tot_cx = 0
        do y=1, dx
        tot_cx = tot_cx + int( sqrt( dx**2 - y**2 )+0.5)
        end do
        divider = (tot_cx*4) -1

c       big ugly loop across i(lon), j(lat)
c       cx = horizontal distance from circle centre (+0.5 for rounding)
c       y = vertical perturbation from cirlce centre
c       ii = loop over horizontal distance from circle centre, out to cx
c       as everything has been summed over the same size cirlces divide
c       everything by a uniform number
        do i=1,nx
            do j=(dx+1),(ny-dx-1)
            tempv = 0.0
            y=1
            cx =   int( sqrt( dx**2 - y**2 )+0.5)
            tempv = tempv + sum( indata( (i-cx):(i+cx) , (j+(y-1))  ))
                do y=2,dx
                cx =   int( sqrt( dx**2 - y**2 )+0.5)
                    do ii=(i-cx), (i+cx)
                        iii = ii
                        if(iii.lt.1)then
                            iii = iii+nx
                        end if
                        if(iii.gt.nx)then
                            iii = iii-nx
                        end if
        tempv = tempv + indata( iii , (j+(y-1)) )
        tempv = tempv + indata( iii , (j-(y-1)) )
                    end do
                enddo
       bkgrd(i,j) = tempv / divider
        tempv = 0.0
            enddo
        enddo
        bkgrd(:,1:dxi) = 9.96921e+36
        bkgrd(:,(ny-dxi):ny) = 9.96921e+36  !This if NCLs Float Missing Val.
        return
        end
