C NCLFORTSTART
      subroutine boot_average (indata, avgdata,h,nt,nb,nx,ny)
C indata = Raw (time, lat, lon)
C avgdata = boots average(boots, lat ,lon)
C random_ind = random_indices generated in ncl
c h = times of indata
c nt = times to be sampled
c nb = number of boots to do, usually 1000
c nx = nlon
c ny = nlat
      real indata(nx,ny,h), avgdata(nx,ny,nb)
      integer ro(nt,nb),nb,nt,nx,ny,b,h
C NCLEND
C
C
        print *, "+ + + + + + + + + + + + + + + + +"
        print *, " + + + + + + + + + + + + + + + +"
        print *, "+ + + + + + + + + + + + + + + + +"
        print *, "Starting Creating Averages"
        
      call gen_random_number(nt, nb, h, ro)
      avgdata = 0.0

      do 11 b=1,(nb-1)
        if ( MOD(b,100) == 0 ) then
            print *, (b*100)/nb,"%"
         end if
        do 10 i=1,ny
           avgdata(:,:,b) =  avgdata(:,:,b) + indata(:,:,ro(i,b))
   10   continue
   11 continue
      print *, " Done Creating Many Averages"
        avgdata = avgdata / nt
        
      do 12 i=1,nx
        do 13 j=1,ny
            call HPSORT( nb, avgdata(i,j,:) )
   13     continue
   12  continue
      return
      end
      
       
       subroutine gen_random_number(nt, nb,high, ro)
       integer :: values(1:8), k, ro(nt,nb), nt, nb,high
       integer, dimension(:), allocatable :: seed
       real(4) :: r(nt,nb)
       
       call date_and_time(values=values)
       call random_seed(size=k)
       allocate(seed(1:k))
       seed(:) = values(8)
       call random_seed(put=seed)
       call random_number(r)
       ro = int(r*high)
       ro = ro + 1.0
c  shifts this as fortran is base 1
       return
       end
       
       
       
       
       !*****************************************************
!*  Sorts an array RA of length N in ascending order *
!*                by the Heapsort method             *
!* ------------------------------------------------- *
!* INPUTS:                                           *
!*	    N	  size of table RA                   *
!*          RA	  table to be sorted                 *
!* OUTPUT:                                           *
!*	    RA    table sorted in ascending order    *
!*                                                   *
!* NOTE: The Heapsort method is a N Log2 N routine,  *
!*       and can be used for very large arrays.      *
!*****************************************************         
       SUBROUTINE HPSORT(N,RA)
       real RA(N)
       L=N/2+1
       IR=N
       !The index L will be decremented from its initial value during the
       !"hiring" (heap creation) phase. Once it reaches 1, the index IR 
       !will be decremented from its initial value down to 1 during the
       !"retirement-and-promotion" (heap selection) phase.
10     continue
       if(L > 1)then
         L=L-1
         RRA=RA(L)
       else
         RRA=RA(IR)
         RA(IR)=RA(1)
         IR=IR-1
         if(IR.eq.1)then
           RA(1)=RRA
           return
         end if
       end if
       I=L
       J=L+L
20     if(J.le.IR)then
       if(J < IR)then
         if(RA(J) < RA(J+1))  J=J+1
       end if
       if(RRA < RA(J))then
         RA(I)=RA(J)
         I=J; J=J+J
       else
         J=IR+1
       end if
       goto 20
       end if
       RA(I)=RRA
       goto 10
       END

