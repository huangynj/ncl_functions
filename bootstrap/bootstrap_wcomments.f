	 subroutine dim_avg_o(datin,retdata, nx, ny,nt, FILL)
	 integer :: x, y, nx, ny, nt,t
	 real :: datin(nx,ny,nt), retdata(nx,ny)
	 integer ct(nx,ny)
	real start, finish

	 retdata = 0
	 ct=0
	 do t=1,nt
	 do y=1,ny
	 do x=1,nx
	     if( (datin(x,y,t) .NE. FILL)   )then
	       retdata(x,y) = retdata(x,y)+ datin(x,y,t)
	       ct(x,y) = ct(x,y) +1
              end if
	    end do
	  end do
	end do

	do y=1,ny
        do x=1,nx
        if(ct(x,y) .EQ. 0)then
          retdata(x,y) = FILL
        else
          retdata(x,y) = retdata(x,y)/ct(x,y)
        end if
	end do
	end do
!	 retdata = retdata / nt
	 return
	 end



!! Fancy array based method is slower than simple loop method
         subroutine dim_avg(datin,retdata, nx, ny,nt, FILL)
         integer :: x, y, nx, ny, nt,t,ct
         real :: datin(nx,ny,nt), retdata(nx,ny)
	 logical:: mask(nx,ny,nt)
!         retdata = FILL
!         do y=1,ny
!         do x=1,nx

!	 retdata(x,y)=SUM(datin(x,y,:), MASK=ABS(datin(x,y,:)-FILL)>0.1)
!     +  /COUNT((datin(x,y,:)-FILL)>0.1)
	 mask = ABS(datin - FILL) > 0.1
         retdata=SUM(datin,3, mask)/COUNT(mask, 3)
 !          end do
 !        end do
!        retdata = retdata / nt
         return
         end



          subroutine init_random_seed()
       !     use iso_fortran_env, only: int64
            implicit none
            integer, allocatable :: seed(:)
            integer :: i, n, un, istat, dt(8), pid
            integer :: t, u

            call random_seed(size = n)
            allocate(seed(n))
            ! First try if the OS provides a random number generator
         open(unit=un, file="/dev/urandom", access="stream",
     +   form="unformatted", action="read", status="old", iostat=istat)
            if (istat == 0) then
               read(un) seed
               close(un)
            else
               ! Fallback to XOR:ing the current time and pid. The PID is
               ! useful in case one launches multiple instances of the same
               ! program in parallel.
               call system_clock(t)
               if (t == 0) then
                  call date_and_time(values=dt)
                  t = (dt(1) - 1970) * 365 * 24 * 60 * 60 * 1000
     +                  + dt(2) * 31 * 24 * 60 * 60 * 1000
     +                   + dt(3) * 24 * 60 * 60 * 1000
     +                   + dt(5) * 60 * 60 * 1000
     +                   + dt(6) * 60 * 1000 + dt(7) * 1000
     +                   + dt(8)
               end if
               pid = getpid()
               t = ieor(t, int(pid, kind(t)))
               seed = t
            end if
	    call random_seed(put=seed)
          end subroutine init_random_seed


	 subroutine gen_random_number(nt, nb,high, ro)
	 integer :: values(1:8), k, ro(2,nt,nb), nt, nb,high(2)
	 integer, dimension(:), allocatable :: seed
	 real(4) :: r(2,nt,nb)

c	 call date_and_time(values=values)
c	 call random_seed(size=k)
c	 allocate(seed(1:k))
c	 seed(:) = values(:)
c	 call random_seed(put=seed)

	 call init_random_seed()
	 call random_number(r)

	 ro(1,:,:) = int(r(1,:,:)*high(1))
	 ro(2,:,:) = int(r(2,:,:)*high(2))
	 !print*, ro(1,1,:), ro(2,1,:)

	 ro = ro + 1.0
c  shifts this as fortran is base 1
	 return
	 end


C NCLFORTSTART
	 subroutine bootstrap(posdata,negdata,pdata,np,nn,nx,ny,ntests,F)
	 real posdata(nx,ny,np), negdata(nx,ny,nn), retdata(nx,ny)
	 real randSamp(nx,ny), randComp(nx,ny), pdata(nx,ny), F
	 integer nx,ny,np,nn, ntests,i, x, y,t
	 integer rn(np,ntests,2), ct2(nx,ny,2),ct(nx,ny)
	logical mask(nx,ny,np)
	real start, finish
C NCLEND
!	 print*, MAXVAL(MAXVAL(MAXVAL(posdata, 1),1),1)
!	 print*, MAXVAL(MAXVAL(MAXVAL(negdata, 1),1),1)
	 !! working on there being more or equal negative than positive cases
	 call gen_random_number(np, ntests, (/nn,np/), rn)
	! call gen_random_number(np, ntests, np, rp)


	 retdata = 0
	  do i=1,ntests

C          call cpu_time(start)
C	   call dim_avg_o( posdata(:,:,rn(:,i,1)),randSamp,nx,ny, np, F)
C	   call dim_avg_o( negdata(:,:,rn(:,i,2)),randComp,nx,ny, np, F)
C           call cpu_time(finish)
C           print '("Sub Time = ",f6.3," seconds.")',finish-start


         randSamp = 0
         ct=0
         do t=1,np
         do y=1,ny
         do x=1,nx
             if( (posdata(x,y,rn(t,i,1)) .NE. FILL)   )then
               randSamp(x,y) = randSamp(x,y)+ posdata(x,y,rn(t,i,1))
               ct(x,y) = ct(x,y) +1
              end if
            end do
          end do
        end do

      where(ct .EQ. 0)
        randSamp = FILL
      elsewhere
        randSamp = randSamp/ct
      end where

C        do y=1,ny
C        do x=1,nx
C        if(ct(x,y) .EQ. 0)then
C          randSamp(x,y) = FILL
C        else
C          randSamp(x,y) = randSamp(x,y)/ct(x,y)
C        end if
C        end do
C        end do

         randComp = 0
         ct=0
         do t=1,np
         do y=1,ny
         do x=1,nx
             if( (negdata(x,y,rn(t,i,2)) .NE. FILL)   )then
               randComp(x,y) = randComp(x,y)+ negdata(x,y,rn(t,i,2))
               ct(x,y) = ct(x,y) +1
              end if
            end do
          end do
        end do
C
C       call cpu_time(start)
C
C        do y=1,ny
C          do x=1,nx
C            if(ct(x,y) .EQ. 0)then
C              randComp(x,y) = FILL
C            else
C              randComp(x,y) = randComp(x,y)/ct(x,y)
C            end if
C          end do
C        end do
C
C            call cpu_time(finish)
C           print '("inline Time = ",f6.3," seconds.")',finish-start
C
C      call cpu_time(start)

      where(ct .EQ. 0)
        randComp = FILL
      elsewhere
        randComp = randComp/ct
      end where
C           call cpu_time(finish)
C           print '("inline Time = ",f6.3," seconds.")',finish-start


C         call cpu_time(start)


	  where( randSamp > randComp)
	   retdata = retdata + 1
	  end where
C	        call cpu_time(finish)
C           print '("where Time = ",f6.3," seconds.")',finish-start
C
C         call cpu_time(start)
C
C	   do y=1,ny
C       	   do x=1,nx
C		 if(randSamp(x,y) > randComp(x,y) )then
C		    retdata(x,y) = retdata(x,y)+1
C		 end if
C	   end do
C	   end do
C	           call cpu_time(finish)
C           print '("loop Time = ",f6.3," seconds.")',finish-start

C  where(randSamp > randComp) retdata = retdata+1
	  end do

      retdata = retdata/ntests
!	 do y=1,ny
!         do x=1,nx
!		 if(retdata(x,y) > (1-retdata(x,y)) )then
!		    pdata(x,y) = -1 + 2*retdata(x,y)
!        else
!        	pdata(x,y) = -1 + 2*(1-retdata(x,y))
!		 end if
!	    end do
!	   end do
	 where( retdata > (1-retdata) )
	  pdata = -1 + 2*retdata
	 elsewhere
	  pdata = -1 + (2*(1-retdata))
	 end where

!    print*, "tits"
!	 return
	 end



