	 subroutine dim_avg_o(datin,retdata, nx, ny,nt, FILL)
	 integer :: x, y, nx, ny, nt,t
	 real :: datin(nx,ny,nt), retdata(nx,ny)
	 integer ct(nx,ny)
	real start, finish

	 retdata = 0
	 ct=0
	 do t=1,nt
	 do y=1,ny
	 do x=1,nx
	     if( (datin(x,y,t) .NE. FILL)   )then
	       retdata(x,y) = retdata(x,y)+ datin(x,y,t)
	       ct(x,y) = ct(x,y) +1
              end if
	    end do
	  end do
	end do

	do y=1,ny
        do x=1,nx
        if(ct(x,y) .EQ. 0)then
          retdata(x,y) = FILL
        else
          retdata(x,y) = retdata(x,y)/ct(x,y)
        end if
	end do
	end do
	 return
	 end



!! Fancy array based method is slower than simple loop method
         subroutine dim_avg(datin,retdata, nx, ny,nt, FILL)
         integer :: x, y, nx, ny, nt,t,ct
         real :: datin(nx,ny,nt), retdata(nx,ny)
	 logical:: mask(nx,ny,nt)

	 mask = ABS(datin - FILL) > 0.1
         retdata=SUM(datin,3, mask)/COUNT(mask, 3)
         return
         end



          subroutine init_random_seed()
       !     use iso_fortran_env, only: int64
            implicit none
            integer, allocatable :: seed(:)
            integer :: i, n, un, istat, dt(8), pid
            integer :: t, u

            call random_seed(size = n)
            allocate(seed(n))
            ! First try if the OS provides a random number generator
         open(unit=un, file="/dev/urandom", access="stream",
     +   form="unformatted", action="read", status="old", iostat=istat)
            if (istat == 0) then
               read(un) seed
               close(un)
            else
               ! Fallback to XOR:ing the current time and pid. The PID is
               ! useful in case one launches multiple instances of the same
               ! program in parallel.
               call system_clock(t)
               if (t == 0) then
                  call date_and_time(values=dt)
                  t = (dt(1) - 1970) * 365 * 24 * 60 * 60 * 1000
     +                  + dt(2) * 31 * 24 * 60 * 60 * 1000
     +                   + dt(3) * 24 * 60 * 60 * 1000
     +                   + dt(5) * 60 * 60 * 1000
     +                   + dt(6) * 60 * 1000 + dt(7) * 1000
     +                   + dt(8)
               end if
               pid = getpid()
               t = ieor(t, int(pid, kind(t)))
               seed = t
            end if
	    call random_seed(put=seed)
          end subroutine init_random_seed


	 subroutine gen_random_number(nt, nb,high, ro)
	 integer :: values(1:8), k, ro(2,nt,nb), nt, nb,high(2)
	 integer, dimension(:), allocatable :: seed
	 real(4) :: r(2,nt,nb)

	 call init_random_seed()
	 call random_number(r)

	 ro(1,:,:) = int(r(1,:,:)*high(1))
	 ro(2,:,:) = int(r(2,:,:)*high(2))

	 ro = ro + 1.0
c  shifts this as fortran is base 1
	 return
	 end


C NCLFORTSTART
	 subroutine bootstrap(posdata,negdata,pdata,np,nn,nx,ny,ntests,F)
	 real posdata(nx,ny,np), negdata(nx,ny,nn),pdata(nx,ny) 
	 real retdata(nx,ny)
	 real randSamp(nx,ny), randComp(nx,ny),  F
	 integer nx,ny,np,nn, ntests,i, x, y,t
	 integer rn(np,ntests,2), ct(nx,ny)
C NCLEND
	 call gen_random_number(np, ntests, (/nn,np/), rn)



	
!	print*,"Random Numbers Generated"
        retdata = 0
	pdata = 0
	randSamp =0
	randComp =0
!        print*, "Lets start looping"
	do i=1,ntests  !! boot test loop

!!! First calc mean of random postive data
         randSamp = 0
         ct=0
!	print*, "Lets start inner looping"
         do t=1,np
         do y=1,ny
         do x=1,nx
             if( (posdata(x,y,rn(t,i,1)) .NE. FILL)   )then
               randSamp(x,y) = randSamp(x,y)+ posdata(x,y,rn(t,i,1))
               ct(x,y) = ct(x,y) +1
              end if
            end do
          end do
        end do
!      print*,"pos mean calcd"
      where(ct .EQ. 0)
        randSamp = FILL
      elsewhere
        randSamp = randSamp/ct
      end where
!	print*,i
!! Then calc mean of random negative data
         randComp = 0
         ct=0
         do t=1,np
         do y=1,ny
         do x=1,nx
             if( (negdata(x,y,rn(t,i,2)) .NE. FILL)   )then
               randComp(x,y) = randComp(x,y)+ negdata(x,y,rn(t,i,2))
               ct(x,y) = ct(x,y) +1
              end if
        end do
        end do
        end do
!	print*, "neg mean calcd"
      where(ct .EQ. 0)
        randComp = FILL
      elsewhere
        randComp = randComp/ct
      end where
!	print*,i
!!!  Then increment retdata where pos mean is greater than neg mean
      where( randSamp > randComp)
       retdata = retdata + 1
      end where

      end do  !!! end boot test loop

      retdata = retdata/ntests  !! make retdata 0-1 fraction


!! some weird logic to get two tailed fraction.
	 where( retdata > (1-retdata) )
	  pdata = -1 + 2*retdata
	 elsewhere
	  pdata = -1 + (2*(1-retdata))
	 end where

	 end



