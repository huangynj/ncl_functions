;load "~/NCL/loadFiles.ncl"

undef("calc_okubo_weiss_interim")
function calc_okubo_weiss_interim(intime,pres)
begin
ut = cd_calendar(intime,-5)

inYear = sprinti("%0.4i",ut(:,0))
inMn  = sprinti("%0.2i",ut(:,1))
uF = addfile("/ecmwf/interim/"+inYear+"/u."+inYear+inMn+".grib","r")
vF =  addfile("/ecmwf/interim/"+inYear+"/v."+inYear+inMn+".grib","r")



u = uF->U_GDS0_ISBL({intime},{700},::-1,:)
v = vF->V_GDS0_ISBL({intime},{700},::-1,:)

ux = u
uy = u

vx = v
vy = v

gradsf(u,ux,uy)
gradsf(v,vx,vy)

ow = ((vx-uy)^2) - ((ux-vy)^2) - ((vx+uy)^2)
ow = ow*10^9

copy_VarMeta(u,ow)
printVarSummary(ow)

;ow1 = dim_sum_n_Wrap(ow,0)


return(ow)
end

undef("calc_ow")
function calc_ow(u,v,opt)
begin

ux = u
uy = u

vx = v
vy = v

gradsf(u,ux,uy)
gradsf(v,vx,vy)

ow = ((vx-uy)^2) - ((ux-vy)^2) - ((vx+uy)^2)
ow = ow*10^9

copy_VarMeta(u,ow)
ow@long_name = "Okubo Weiss"
ow@units = "10~S~-9~N~ s~S~-1"

ow = where( (vx-uy).gt.0, ow, 0)  ;;  KEY PART ONLY PLOT WHERE CYCLONIC VORT!!!
;ow1 = dim_sum_n_Wrap(ow,0)
return(ow)
end


undef("uv2owF_Wrap")
function uv2owF_Wrap(u,v)
begin

ux = u
uy = u

vx = v
vy = v

gradsf(u,ux,uy)
gradsf(v,vx,vy)

ow = ((vx-uy)^2) - ((ux-vy)^2) - ((vx+uy)^2)
ow = ow*10^9

copy_VarMeta(u,ow)
ow@long_name = "Okubo Weiss"
ow@units = "10~S~-9~N~ s~S~-1"

ow = where( (vx-uy).gt.0, ow, 0)  ;;  KEY PART ONLY PLOT WHERE CYCLONIC VORT!!!

return(ow)
end




undef("calc_ow_inv")
function calc_ow_inv(u,v)
begin

ux = u
uy = u

vx = v
vy = v

gradsf(u,ux,uy)
gradsf(v,vx,vy)

ow =   (   ( ( ((ux-vy)^2) + ((vx+uy)^2) )^2)    -   ((vx-uy)^2)  );
   
ow = -ow*10^9   ; (Okubo 1970, Weiss 1991) strain perspective, make negative for rotation dominance

copy_VarMeta(u,ow)
printVarSummary(ow)
ow@long_name = "Okubo Weiss"
ow@units = "10~S~-9~N~ s~S~-1"

ow = where( (vx-uy).gt.0, ow, 0) ;;  KEY PART ONLY PLOT WHERE CYCLONIC VORT!!!

;ow1 = dim_sum_n_Wrap(ow,0)
return(ow)
end



undef("calc_okubo_weiss2")
function calc_okubo_weiss2(u,v)
begin

ux = u
uy = u

vort = uv2vrF_Wrap(u,v)
  vort2 = vort^2

neg_u = -u    
 copy_VarMeta(u,neg_u)
neg_v = -v    
 copy_VarMeta(v,neg_v)

def2 = uv2vrF_Wrap(neg_u,v)
def1 = uv2dvF_Wrap(u,neg_v)

defall2 = def1^2+def2^2     
defall = sqrt(defall2)

ow = (vort2)-(defall2)


copy_VarMeta(u,ow)
printVarSummary(ow)
ow@long_name = "Okubo Weiss"
ow@units = "10~S~-9~N~ s~S~-2"
ow = ow*10^9
;ow1 = dim_sum_n_Wrap(ow,0)


return(ow)
end















undef("dummy_run")
procedure dummy_run()
begin

intime = cd_inv_calendar(2010,09,04,00,00,00,"hours since 1800-01-01 00:00:00",0)

ut = cd_calendar(intime,-5)

inYear = sprinti("%0.4i",ut(:,0))
inMn  = sprinti("%0.2i",ut(:,1))
;uF = addfile("/ecmwf/interim/"+inYear+"/u."+inYear+inMn+".grib","r")
;vF =  addfile("/ecmwf/interim/"+inYear+"/v."+inYear+inMn+".grib","r")

;u = uF->U_GDS0_ISBL({intime},{700},::-1,:)
;v = vF->V_GDS0_ISBL({intime},{700},::-1,:)

;uF = addfile("/free4/abrammer/data/CFSR/"+inYear+"/u."+inYear+inMn+".nc","r")
;vF = addfile("/free4/abrammer/data/CFSR/"+inYear+"/v."+inYear+inMn+".nc","r")

;u = uF->UGRD_P0_L100_GLL0({intime},{70000},::-1,:)
;v = vF->VGRD_P0_L100_GLL0({intime},{70000},::-1,:)



inF = addfile("/free4/abrammer/data/FNL/2010/09/fnl_20100904_00_00.grb","r");

u = inF->UGRD_P0_L100_GLL0({70000},::-1,:)
v = inF->VGRD_P0_L100_GLL0({70000},::-1,:)





wks  = gsn_open_wks("png","~/WEB/ncl_plots/okubo_weiss_fnl")
gsn_define_colormap(wks, "WhiteBlueGreenYellowRed")
res = True

res = w_africa_map(res)

res@gsnDraw = False
res@gsnFrame = False

res@mpMinLonF = -95
res@mpMaxLonF = -10
res@cnFillOn = True
res@lbLabelAutoStride = True
res@gsnSpreadColors = True
res@cnLinesOn = False
res@gsnAddCyclic = True
res@cnLevelSelectionMode = "ExplicitLevels"
res@cnLevels = (/0.5,1,2,3,4,5,6,7,8,9/)

res@tiMainString = cd_calendar(intime,-3)

ow = calc_okubo_weiss_proper(u,v)
plot = gsn_csm_contour_map(wks, ow, res)

ow1 = calc_okubo_weiss(u,v)
plot1 = gsn_csm_contour_map(wks, ow1, res)

gsn_panel(wks, (/plot,plot1/),(/2,1/),False)


end
