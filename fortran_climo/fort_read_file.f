*     789012345678901234567890123456789012345678901234567890123456789012
*     ^  1         2         3         4         5         6         7 ^

      PROGRAM read_file
      PARAMETER (nlon=720)
      PARAMETER (nyr=9)
      integer varid,year,status,vid,ncid,y
      real etime,total,elapsed(2)  
      integer cur_ind
      character yrstr*4
      real out_var(nlon, 1460,nyr)
      real avg_dat(nlon,  1460)
      real lons(720), lats(361)
      integer outstart(3), outcount(3)
      DATA outstart /1,1,1/
      DATA outcount /nlon,1,1460/
      integer fileid,myvar,dd(3),varidx,dimidx,dimidt,dimidy
*     netCDF functions
c      integer NF_CREATE,NF_CLOBBER,NF_DEF_DIM,NF_REAL,NF_ENDDEF
c      integer NF_PUT_VARA_REAL,NF_CLOSE, 
      include 'netcdf.inc'
      
      avg_dat = 10.0
      do i=0,360
      lats(i+1) = -90+(i*0.5)
      end do
      
      do i=1,720
      lons(i) = (i-1)/2.0
      end do
      
      status=NF_CREATE('/ct12/abrammer/Test2.nc',NF_CLOBBER,fileid)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      status=NF_DEF_DIM(fileid,'lat',361,dimidy)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      status=NF_DEF_DIM(fileid,'lon',720,dimidx)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      status=NF_DEF_DIM(fileid,'time',1460,dimidt)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      dd(1)=dimidx
      err=NF_DEF_VAR(fileid,'lon',NF_REAL,1,dd,varidx)
            IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      dd(1)=dimidy
      err=NF_DEF_VAR(fileid,'lat',NF_REAL,1,dd,varidy)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)

      dd(1)=dimidx
      dd(2)=dimidy
      dd(3)=dimidt
      status=NF_DEF_VAR(fileid,'MyVar',NF_REAL,3,dd,myvar)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      status=NF_ENDDEF(fileid)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      
      status=NF_PUT_VARA_REAL(fileid,varidx,1,720,lons)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      status=NF_PUT_VARA_REAL(fileid,varidy,1,361,lats) 
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)

      
      
      print*,'*************************'
      
      do lat=1,40
      outstart(2) = lat
      print*,lat   
      print*,outstart
      print*,outcount

      cur_ind = 0
       do 10 year = 1979, 1989
      call load_data(year, out_var, cur_ind,lat)
10    continue

        print*, 'Starting Average'
      call avg_data(out_var,avg_dat)
      
            print*, 'Starting Write'             
      status=NF_PUT_VARA_REAL(fileid,myvar,outstart,outcount,avg_dat)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)   
      status=NF_CLOSE(fileid)
              IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)      
      
      
      
      total = etime(elapsed)
      print *, 'End: total=', total, ' user=', elapsed(1),
     &         ' system=', elapsed(2)      

      end do


      total = etime(elapsed)
      print *, 'End: total=', total, ' user=', elapsed(1),
     &         ' system=', elapsed(2)      

c        ret_var = 10
        print*, '*********************'
      end


      subroutine load_data(year, out_var, cur_ind,lat)
      PARAMETER (nlon=720)
      PARAMETER (nyr=1)
      integer lat
      integer year, ncid,status,leap,t_ind, cur_ind,y
      character yrstr*4
      real out_var(nlon,1460,nyr)
      INTEGER START(4), COUNT(4),COUNTL(4)
      real temp_var(nlon, 1460)!, temp_varL(2,1464)
      include 'netcdf.inc'
      write(yrstr,'(I4)') year  
      DATA START /1,1,1,1/
      DATA COUNT /nlon, 1, 1,1460/
      DATA COUNTL /2, 1, 1,1460/
      
      START(2) = lat
      
      status=nf_open('/cfsr/data/'//yrstr//'/v.'//yrstr//'.0p5.anl.nc',
     & NF_NOWRITE, ncid)
        IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
      status=NF_INQ_VARID(ncid,"v",vid)	! Retrieve Variable Id
        IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
        
        t_ind = cur_ind+1
        leap = mod(year,4)
        y=year-1978
        print*, year, leap
      if( leap .eq. 5) then
      print*, START
      print*,COUNTL
       status=NF_GET_VARA_REAL(ncid,vid,START, COUNTL, temp_varL)
           IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
c        cur_ind = cur_ind+COUNTL(4)
c        out_var(:,t_ind:cur_ind,1) = temp_varL
      else  
        status=NF_GET_VARA_REAL(ncid,vid,START, COUNT, temp_var)
          IF (status .NE. NF_NOERR)  CALL HANDLE_ERR(status)
        cur_ind = cur_ind+COUNT(4)
        out_var(:,1:1460,y) = temp_var
      end if
c     print*, t_ind, cur_ind      
      end
      
      
      subroutine avg_data(out_var)
      PARAMETER (nlon=720)
      PARAMETER (nyr=1)
      real out_var(nlon,1460,nyr)
      real avg_dat(nlon,1460)
      real sum_temp
      integer ts, y, i,t,tm
            
            
      do 23 i=1,nlon
      do 22 ts=1,1460     ! this is the time indice to start with
        sum_temp = 0.0
      do 21 t=ts-40,tm+40,4   ! call +- 21 days every 4th indice
      do 20 y=2,nyr-1            ! loop thru all years
         if(t.le.0)then       ! for the first 21 days loop over previous yr
           sum_temp = sum_temp + out_var(i,t+1460,y-1)
         else if(t.gt.1460)then
         sum_temp = sum_temp + out_var(i,t-1460,y+1)
         else
           sum_temp = sum_temp + out_var(i,t,y)
         end if
20    continue
21    continue
      
      avg_dat(i,ts) = sum_temp/(nyr-2)*21 ! 21 days for 30 years should be 630 days
22    continue    
23    continue

      end
      
      
      
      
      
      
      
      

cload "~/NCL/loadFiles.ncl"
cinf = addfile("/cfsr/data/2010/v.2010.0p5.anl.nc","r")
cdate = systemfunc("date")
cv = inf->v(:,0,:,0)
cwallClockElapseTime(date, "title",0)



C ###################################################################################################
C     HANDLE_ERR
C ###################################################################################################
      SUBROUTINE HANDLE_ERR(info)
c     Handles NETCDF errors
      implicit none
      include 'netcdf.inc'
      integer info
      IF (info .NE. NF_NOERR) THEN
       PRINT *, NF_STRERROR(info)
       STOP 'Sorry, program stopped. Check the NETCDF error above.'
      ENDIF
      END
