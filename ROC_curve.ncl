undef("AUC")
function AUC(Xin, Yin)
local n, Xin,Yin,xi, X,Y,Sum
begin
n = dimsizes(Xin)
if(n .ne. dimsizes(Yin))
 print("Error X and Y must have same dimensions")
 print("Returning Fill value")
 return( new(1, float) )
end if

xi = dim_pqsort(Xin,1)

X = Xin(xi)
Y = Yin(xi)

Sum = 0.
do i=1, n-1
 Sum =  Sum + (X(i)-X(i-1)) * ((Y(i)+Y(i-1))/2.)
end do

return(Sum)
end



undef("ROC")
function ROC(Obs, Pred, n)
local Pred,thresh,Obs,roc
begin
roc := new( (/n,2/), float)

do i=1, n-2
thresh = ((1/(n*1.))*i)
roc(i,0) = num( Pred.ge.thresh .and. Obs.eq.0)/ (num(Obs.eq.0)*1.)
roc(i,1) = num( Pred.ge.thresh .and. Obs.eq.1)/ (num(Obs.eq.1)*1.)
end do
roc(0,:) = 1.
roc(n-1,:) = 0.

roc@AUC =AUC(roc(:,0), roc(:,1))  ;  x axis is evenly spaced from 0-1 so average = AUC

return( roc )
end


