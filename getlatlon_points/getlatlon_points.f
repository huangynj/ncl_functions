C NCLFORTSTART
      subroutine getlatlon_points(grid, ny, nx,ret,lati, loni, npts)
        integer ny, nx, npts, lati(npts), loni(npts)
        real grid(nx, ny), ret(npts)
C NCLEND
        do i=1,npts
            ret(i) = grid(loni(i)+1, lati(i)+1 )
        end do
      return
      end
