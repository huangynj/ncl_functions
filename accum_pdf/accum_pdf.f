c  NCLFORTSTART
      subroutine accum_pdf(grid,ntime,nlat,nlon, bins,nbins, retval )
      integer ntime, nlat, nlon, nbins,i,j,b,y
      real grid(nlon,nlat,ntime), retval(nbins), bins(nbins)
c  NCLEND
      do i = 1,nlon
      do j = 1,nlat
      do y = 1,ntime
      do b = 1,nbins-1
       if(grid(i,j,y).GE.bins(b).AND. grid(i,j,y).LT.bins(b+1) )then
       retval(b) = retval(b) + grid(i,j,y)
       end if
      end do
      end do
      end do
      end do
      return
      end subroutine